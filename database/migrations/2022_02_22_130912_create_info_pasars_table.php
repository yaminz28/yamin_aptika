<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfoPasarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_pasars', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_pasar')->nullable();
            $table->foreignId('id_komonditas')->nullable();
            $table->integer('harga_komonditas')->nullable();
            $table->datetime('waktu')->nullable();
            $table->string('latitude')->nullable(20);
            $table->string('longtitude')->nullable(20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_pasars');
    }
}
