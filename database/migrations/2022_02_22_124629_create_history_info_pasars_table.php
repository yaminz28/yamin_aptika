<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryInfoPasarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_info_pasars', function (Blueprint $table) {
             $table->id();
            $table->date('tanggal')->nullable();
            $table->integer('id_komonditas')->nullable();
            $table->integer('harga_max')->nullable();
            $table->integer('pasar1')->nullable();
            $table->integer('harga_min')->nullable();
            $table->integer('pasar2')->nullable();
            $table->integer('harga_avg')->nullable();
            $table->integer('last_harga')->nullable();
            $table->integer('pasar3')->nullable();
            $table->foreignId('user_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_info_pasars');
    }
}
