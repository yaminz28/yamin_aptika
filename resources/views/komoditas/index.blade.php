<div class="h-screen w-screen flex items-center justify-center bg-gray-200 ">
    <link rel="stylesheet" href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css">
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Psb') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mb-10">
                <a href="{{ route('psb.create') }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">
                    + Create Komoditas
                </a>
            </div>
            <div class="bg-white">
                <table class="table-auto w-full">
                    <thead>
                    <tr>
                        <th class="border px-6 py-4">Jenis Komoditas</th>
                        <th class="border px-6 py-4">Harga</th>
                        <th class="border px-6 py-4">Foto</th>
                        <th class="border px-6 py-4">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($Psb as $item)
                            <tr>
                                <td class="border px-6 py-4">{{ $item->id }}</td>
                                <td class="border px-6 py-4 ">{{ $item->name }}</td>
                                {{-- <td class="border px-6 py-4 ">{{ $item->name_panggilan }}</td> --}}
                                <td class="border px-6 py-4 ">{{ $item->tanggal_lahir }}</td>
                                <td class="border px-6 py-4 ">{{ $item->jk }}</td>
                                {{-- <td class="border px-6 py-4 ">{{ $item->agama }}</td>
                                <td class="border px-6 py-4 ">{{ $item->kewarganegaraan }}</td> --}}
                                <td class="border px-6 py-4 ">{{ $item->berat }}</td>
                                <td class="border px-6 py-4 ">{{ $item->tinggi }}</td>
                                <td class="border px-6 py-4 ">{{ $item->golongan_darah }}</td>
                                {{-- <td class="border px-6 py-4 ">{{ $item->penyakit }}</td> --}}
                                <td class="border px-6 py-4 ">{{ $item->alamat }}</td>
                                <td class="border px-6 py-4"><img src="/image/{{ $item->image }}" width="100px"></td>
                                {{-- <td class="border px-6 py-4 ">{{ $item->nama_bpk }}</td>
                                <td class="border px-6 py-4 ">{{ $item->nama_ibu }}</td>
                                <td class="border px-6 py-4 ">{{ $item->pendidikan_bpk }}</td>
                                <td class="border px-6 py-4 ">{{ $item->pendidikan_ibu }}</td>
                                <td class="border px-6 py-4 ">{{ $item->pekerjaan_bpk }}</td>
                                <td class="border px-6 py-4 ">{{ $item->pekerjaan_ibu }}</td> --}}
                                <td class="border px-6 py-4 ">
                                <a href="{{ route('psb.edit', $item->id) }}" class="inline-block bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mx-2 rounded">
                                        Edit
                                    </a>
                                    <form action="{{ route('psb.destroy', $item->id) }}" method="POST" class="inline-block">
                                        {!! method_field('delete') . csrf_field() !!}
                                        <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 mx-2 rounded inline-block">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                               <td colspan="6" class="border text-center p-5">
                                   Data Tidak Ditemukan
                               </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="text-center mt-5">
                {{ $Psb->links() }}
            </div>
        </div>
    </div>
</x-app-layout>
