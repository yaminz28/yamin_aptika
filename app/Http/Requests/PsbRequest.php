<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PsbRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string','alpha', 'max:255'],
            'jk'=> 'required|max:10|',
            'berat'=> 'required|max:255|numeric',
            'tinggi'=> 'required|max:255|numeric',
            'golongan_darah'=> 'required|max:255|alpha',
            'alamat'=> 'required|max:255|alpha_num',
            'tanggal_lahir' => 'required|date',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //
        ];
    }
}
